#!/bin/sh
# Usage: ./build.sh [theme]
if [ ! -n "$1" ]; then
  echo "Usage: sh build.sh <theme>"
  exit 1;
fi

if [ -f scss/$1.scss ]; then
  theme="$1"
else
  echo "Invalid theme, choose an available one:" `find scss -type f -exec basename {} .scss \;`
  exit 1;
fi

alias node-sass="node node_modules/node-sass/bin/node-sass"
echo "Cleaning up the dist directory..."
rm -rf dist

echo "Generating main.css..."
node-sass --source-map true --output-style expanded --output dist/expanded  scss/${theme}.scss
mv dist/expanded/${theme}.css dist/main.css
mv dist/expanded/${theme}.css.map dist/main.map
rm -r dist/expanded
sed -i -e 's/${theme}.css.map/main.map/g' dist/main.css

echo "Generating main.min.css..."
node-sass --source-map true --output-style compressed --output dist/minified scss/${theme}.scss
mv dist/minified/${theme}.css dist/main.min.css
mv dist/minified/${theme}.css.map dist/main.min.map
rm -r dist/minified
sed -i -e 's/${theme}.css.map/main.min.map/g' dist/main.min.css
