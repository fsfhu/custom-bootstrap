# FSF.hu egyéni Bootstrap témák

A projekt tartalmazza a weboldalaink által használt egyéni Bootstrap témákat.

## Használat

    npm install
    sh build.sh <téma>

A kimeneti fájlok a `dist/` mappába kerülnek.
